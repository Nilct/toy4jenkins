package libs

import (
	"encoding/json"
	"fmt"
	"os"
)

// Configuration desciption of json file
type Configuration struct {
	Runs int    `json:"runs"`
	Fail string `json:"fail"`
}

// ReadConfig read file
func ReadConfig(filename string) (*Configuration, error) {
	// read file
	file, err := os.Open(filename)
	if err != nil {
		fmt.Println("FAIL to read config file")
		fmt.Println("error:", err)
		return nil, err
	}
	// decode file
	decoder := json.NewDecoder(file)
	configuration := Configuration{}
	err = decoder.Decode(&configuration)
	if err != nil {
		fmt.Println("FAIL to decode config file")
		fmt.Println("error:", err)
		return nil, err
	}
	return &configuration, nil
}

// CheckRuns check runs value
func CheckRuns(config *Configuration) bool {
	return config.Runs > 0
}

// CheckFail check fail value
func CheckFail(config *Configuration) bool {
	return config.Fail == "yes"
}
