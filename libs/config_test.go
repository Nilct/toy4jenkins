package libs

import "testing"

func TestReadConfig(t *testing.T) {
	var files = []string{
		"/data/local/config.json",
		"/data/local/configRuns.json",
		"/data/local/configFail.json",
	}
	for _, n := range files {
		_, err := ReadConfig(n)
		if err != nil {
			t.Errorf("Error for file %s", n)
		}
	}
}
